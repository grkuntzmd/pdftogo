/*
 * MIT LICENSE
 *
 * Copyright © 2017, G.Ralph Kuntz, MD.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package pdf

import (
	"bytes"
	"fmt"
	"sort"
)

// Document contains the parsed PDF.
type Document struct {
	Version         float32
	StartXRefOffset int64
	XRefTable       map[int64][]XRefEntry
}

// NewDocument creates and initializes a new PDF Document.
func NewDocument() *Document {
	return &Document{
		XRefTable: make(map[int64][]XRefEntry),
	}
}

type byValue []int64

func (b byValue) Len() int {
	return len(b)
}

func (b byValue) Less(i, j int) bool {
	return b[i] < b[j]
}

func (b byValue) Swap(i, j int) {
	b[i], b[j] = b[j], b[i]
}

func (d *Document) String() string {
	b := &bytes.Buffer{}
	fmt.Fprintln(b, "Document {")
	fmt.Fprintf(b, "\tVersion: %0.1f\n", d.Version)
	fmt.Fprintf(b, "\tStartXRefOffset: %[1]d (%#[1]x)\n", d.StartXRefOffset)
	fmt.Fprintln(b, "\tXRefTable: {")
	o := make([]int64, 0, len(d.XRefTable))
	for k := range d.XRefTable {
		o = append(o, k)
	}
	sort.Sort(byValue(o))
	for _, k := range o {
		fmt.Fprintf(b, "\t%10d: ", k)
		for _, e := range d.XRefTable[k] {
			fmt.Fprintf(b, "%[1]d (%#[1]x) [%d] ", e.Offset, e.Generation)
		}
		fmt.Fprintln(b)
	}
	fmt.Fprintln(b, "\t}")
	fmt.Fprintln(b, "}")

	return b.String()
}
