/*
 * MIT LICENSE
 *
 * Copyright © 2017, G.Ralph Kuntz, MD.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package parser

import (
	"io"
	"strings"
	"testing"

	"bitbucket.org/grkuntzmd/test"
)

func TestParseToken(t *testing.T) {
	t.Parallel()
	f := func(p *Parser, exp interface{}) {
		s, err := p.parseToken()
		test.OK(t, err)
		test.Equals(t, exp, s)
	}

	t.Run("should parse a non-delimiter token", readTokenHelper("xxxx42()yyyy", "42", 4, 6, f))
	t.Run("should parse a '(' token", readTokenHelper("xxxx()yyyy", "(", 4, 5, f))
	t.Run("should parse a ')' token", readTokenHelper("xxxx)(yyyy", ")", 4, 5, f))
	t.Run("should parse a '<' token", readTokenHelper("xxxx<>yyyy", "<", 4, 5, f))
	t.Run("should parse a '>' token", readTokenHelper("xxxx><yyyy", ">", 4, 5, f))
	t.Run("should parse a '[' token", readTokenHelper("xxxx[]yyyy", "[", 4, 5, f))
	t.Run("should parse a ']' token", readTokenHelper("xxxx][yyyy", "]", 4, 5, f))
	t.Run("should parse a '{' token", readTokenHelper("xxxx{}yyyy", "{", 4, 5, f))
	t.Run("should parse a '}' token", readTokenHelper("xxxx}{yyyy", "}", 4, 5, f))
	t.Run("should parse a '/' token", readTokenHelper("xxxx/yyyy", "/", 4, 5, f))
	t.Run("should parse a non-delimiter token, skipping leading whitespace", readTokenHelper("xxxx  42()yyyy", "42", 4, 8, f))
	t.Run("should parse a non-delimiter token, skipping leading comment", readTokenHelper("xxxx%foo\n42()yyyy", "42", 4, 11, f))
	t.Run("should parse a '(' token, skipping leading whitespace", readTokenHelper("xxxx  ()yyyy", "(", 4, 7, f))
	t.Run("should parse a ')' token, skipping leading whitespace", readTokenHelper("xxxx  )(yyyy", ")", 4, 7, f))
	t.Run("should parse a '<' token, skipping leading whitespace", readTokenHelper("xxxx  <>yyyy", "<", 4, 7, f))
	t.Run("should parse a '>' token, skipping leading whitespace", readTokenHelper("xxxx  ><yyyy", ">", 4, 7, f))
	t.Run("should parse a '[' token, skipping leading whitespace", readTokenHelper("xxxx  []yyyy", "[", 4, 7, f))
	t.Run("should parse a ']' token, skipping leading whitespace", readTokenHelper("xxxx  ][yyyy", "]", 4, 7, f))
	t.Run("should parse a '{' token, skipping leading whitespace", readTokenHelper("xxxx  {}yyyy", "{", 4, 7, f))
	t.Run("should parse a '}' token, skipping leading whitespace", readTokenHelper("xxxx  }{yyyy", "}", 4, 7, f))
	t.Run("should parse a '/' token, skipping leading whitespace", readTokenHelper("xxxx  /yyyy", "/", 4, 7, f))
}

func TestPeekToken(t *testing.T) {
	t.Parallel()
	f := func(p *Parser, exp interface{}) {
		s, err := p.peekToken()
		test.OK(t, err)
		test.Equals(t, exp, s)
	}

	t.Run("should parse a non-delimiter token", readTokenHelper("xxxx42()yyyy", "42", 4, 4, f))
	t.Run("should parse a '(' token", readTokenHelper("xxxx()yyyy", "(", 4, 4, f))
	t.Run("should parse a ')' token", readTokenHelper("xxxx)(yyyy", ")", 4, 4, f))
	t.Run("should parse a '<' token", readTokenHelper("xxxx<>yyyy", "<", 4, 4, f))
	t.Run("should parse a '>' token", readTokenHelper("xxxx><yyyy", ">", 4, 4, f))
	t.Run("should parse a '[' token", readTokenHelper("xxxx[]yyyy", "[", 4, 4, f))
	t.Run("should parse a ']' token", readTokenHelper("xxxx][yyyy", "]", 4, 4, f))
	t.Run("should parse a '{' token", readTokenHelper("xxxx{}yyyy", "{", 4, 4, f))
	t.Run("should parse a '}' token", readTokenHelper("xxxx}{yyyy", "}", 4, 4, f))
	t.Run("should parse a '/' token", readTokenHelper("xxxx/yyyy", "/", 4, 4, f))
	t.Run("should parse a non-delimiter token, skipping leading whitespace", readTokenHelper("xxxx  42()yyyy", "42", 4, 4, f))
	t.Run("should parse a non-delimiter token, skipping leading comment", readTokenHelper("xxxx%foo\n42()yyyy", "42", 4, 4, f))
	t.Run("should parse a '(' token, skipping leading whitespace", readTokenHelper("xxxx  ()yyyy", "(", 4, 4, f))
	t.Run("should parse a ')' token, skipping leading whitespace", readTokenHelper("xxxx  )(yyyy", ")", 4, 4, f))
	t.Run("should parse a '<' token, skipping leading whitespace", readTokenHelper("xxxx  <>yyyy", "<", 4, 4, f))
	t.Run("should parse a '>' token, skipping leading whitespace", readTokenHelper("xxxx  ><yyyy", ">", 4, 4, f))
	t.Run("should parse a '[' token, skipping leading whitespace", readTokenHelper("xxxx  []yyyy", "[", 4, 4, f))
	t.Run("should parse a ']' token, skipping leading whitespace", readTokenHelper("xxxx  ][yyyy", "]", 4, 4, f))
	t.Run("should parse a '{' token, skipping leading whitespace", readTokenHelper("xxxx  {}yyyy", "{", 4, 4, f))
	t.Run("should parse a '}' token, skipping leading whitespace", readTokenHelper("xxxx  }{yyyy", "}", 4, 4, f))
	t.Run("should parse a '/' token, skipping leading whitespace", readTokenHelper("xxxx  /yyyy", "/", 4, 4, f))
}

func TestSkipWhitespace(t *testing.T) {
	t.Parallel()
	f := func(p *Parser, _ interface{}) {
		err := p.skipWhitespace()
		test.OK(t, err)
	}

	t.Run("should skip simple whitespace", readTokenHelper("xxxx  \n\t  \r  \f  \nyyyy", nil, 4, 17, f))
	t.Run("should skip a comment", readTokenHelper("xxxx%  \nyyyy", nil, 4, 8, f))
	t.Run("should skip whitespace and a comment", readTokenHelper("xxxx  \r\n% Foo \n  \ryyyy", nil, 4, 18, f))
}

func readTokenHelper(pdf string, exp interface{}, initialOffset, finalOffset int64, f func(*Parser, interface{})) func(*testing.T) {
	return func(t *testing.T) {
		r := strings.NewReader(pdf)
		p, err := NewParser(r)
		test.OK(t, err)

		_, err = r.Seek(initialOffset, io.SeekStart)
		test.OK(t, err)

		f(p, exp)

		off, err := r.Seek(0, io.SeekCurrent)
		test.OK(t, err)
		test.Equals(t, finalOffset, off)
	}
}
