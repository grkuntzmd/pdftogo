/*
 * MIT LICENSE
 *
 * Copyright © 2017, G.Ralph Kuntz, MD.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package parser

import "strings"

// isDelimiter returns true if the given rune is a PDF delimiter (see 7.2.2 table 2).
func isDelimiter(r rune) bool {
	return isOneOf(r, "()<>[]{}/%")
}

// isEOL returns true if the given rune is one of the end-of-line characters (see 7.2.2 table 1).
func isEOL(r rune) bool {
	return r == '\n' || r == '\r'
}

// isOneOf returns true if the given rune is one of the characters in the given string.
func isOneOf(r rune, set string) bool {
	for _, c := range set {
		if r == c {
			return true
		}
	}

	return false
}

// isWhitespace returns true if the given rune is PDF whitespace (see 7.2.2 table 1).
func isWhitespace(r rune) bool {
	return isOneOf(r, "\x00\t\n\f\r ")
}

// maxInt64 calculates the maximum of 2 int64 numbers.
func maxInt64(a, b int64) int64 {
	if a > b {
		return a
	}

	return b
}

// trimEOL removes the end-of-line character(s) CR and LF from the given string.
func trimEOL(s string) string {
	i := strings.IndexAny(s, "\r\n")
	if i >= 0 {
		return s[:i]
	}

	return s
}
