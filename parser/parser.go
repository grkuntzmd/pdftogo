/*
 * MIT LICENSE
 *
 * Copyright © 2017, G.Ralph Kuntz, MD.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

// All references to the PDF standard refer to ISO 32000-1:2008 available at http://www.adobe.com/content/dam/Adobe/en/devnet/acrobat/pdfs/PDF32000_2008.pdf.

package parser

import (
	"io"
	"log"
	"os"

	"bufio"

	"bitbucket.org/grkuntzmd/pdftogo/pdf"
)

var (
	logger = log.New(os.Stderr, "", log.LstdFlags)
	// maxLengthInt64 = len(strconv.FormatInt(math.MaxInt64, 10))
)

// SetLogger initialize the log.Logger that the parser will use to report warnings.
func SetLogger(l *log.Logger) {
	logger = l
}

// Parser contains the working information to parse the PDF file.
type Parser struct {
	io.ReadSeeker
	Length int64
	*pdf.Document
}

// NewParser creates a new PDF parser.
func NewParser(r io.ReadSeeker) (*Parser, error) {
	// Seek the to end of the PDF to find the length.
	length, err := r.Seek(0, io.SeekEnd)
	if err != nil {
		return nil, err
	}

	return &Parser{
		ReadSeeker: r,
		Length:     length,
		Document:   pdf.NewDocument(),
	}, nil
}

// Parse parses the PDF file and builds a pdf.Document structure.
func (p *Parser) Parse() error {
	if err := p.parseHeader(); err != nil {
		return err
	}

	if err := p.parseXRef(); err != nil {
		return err
	}

	// TODO

	return nil
}

// parseToken parses the token beginning at the current location. A token can be a delimiter (see 7.2.2 table 2) or non-delimiter. Whitespace before the token will be skipped.
func (p *Parser) parseToken() (string, error) {
	count := 0

	if err := p.skipWhitespace(); err != nil {
		return "", err
	}

	off, err := p.Seek(0, io.SeekCurrent)
	if err != nil {
		return "", err
	}

	r := bufio.NewReader(p.ReadSeeker)

	c, s, err := r.ReadRune()
	if err != nil {
		return "", err
	}
	count += s

	if isDelimiter(c) {
		if _, err = p.Seek(off+int64(count), io.SeekStart); err != nil {
			return "", err
		}
		return string(c), nil
	}

	var runes []rune
	for !isDelimiter(c) && !isWhitespace(c) {
		runes = append(runes, c)

		c, s, err = r.ReadRune()
		if err != nil {
			return "", err
		}
		count += s
	}

	// Back up over last character read; it was not part of the token.
	count -= s

	// Seek to the location one character after the token.
	_, err = p.Seek(off+int64(count), io.SeekStart)
	return string(runes), err
}

// peekToken parses the token beginning at the current location, but does not advance the location. A token can be a delimiter (see 7.2.2 table 2) or non-delimiter. Whitespace before the token will be skipped.
func (p *Parser) peekToken() (string, error) {
	off, err := p.Seek(0, io.SeekCurrent)
	if err != nil {
		return "", err
	}

	s, err := p.parseToken()
	if err != nil {
		return "", err
	}

	// Rewind the location back to the starting point.
	if _, err = p.Seek(off, io.SeekStart); err != nil {
		return "", err
	}

	return s, nil
}

// skipWhitespace skips leading whitespace (including comments starting with %) at the current PDF location.
func (p *Parser) skipWhitespace() error {
	count := 0

	// Hold the current location.
	off, err := p.Seek(0, io.SeekCurrent)
	if err != nil {
		return err
	}

	r := bufio.NewReader(p.ReadSeeker)

	c, s, err := r.ReadRune()
	if err != nil {
		return err
	}
	count += s

	for isWhitespace(c) || c == '%' {
		if c == '%' {
			// Skip past the comment.
			c, s, err = r.ReadRune()
			if err != nil {
				return err
			}
			count += s

			for !isEOL(c) {
				c, s, err = r.ReadRune()
				if err != nil {
					return err
				}
				count += s
			}
			continue
		}

		c, s, err = r.ReadRune()
		if err != nil {
			return err
		}
		count += s
	}

	// Back up over last character read; it was not whitespace.
	count -= s

	// Seek to the location one character after the whitespace.
	_, err = p.Seek(off+int64(count), io.SeekStart)
	return err
}
