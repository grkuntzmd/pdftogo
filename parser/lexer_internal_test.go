/*
 * MIT LICENSE
 *
 * Copyright © 2017, G.Ralph Kuntz, MD.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package parser

import (
	"strings"
	"testing"

	"bitbucket.org/grkuntzmd/test"
)

func TestLexer(t *testing.T) {
	t.Parallel()
	t.Run("should parse an integer token", lexerHelper("  42{}", integerKind, "42"))
	t.Run("should parse an integer token with a leading +", lexerHelper("  +42{}", integerKind, "+42"))
	t.Run("should parse an integer token with a leading -", lexerHelper("  -42{}", integerKind, "-42"))

	t.Run("should parse an real token", lexerHelper("  42.0{}", realKind, "42.0"))
	t.Run("should parse an real token with a leading +", lexerHelper("  +42.0{}", realKind, "+42.0"))
	t.Run("should parse an real token with a leading -", lexerHelper("  -42.0{}", realKind, "-42.0"))
	t.Run("should parse an real token with a leading dot", lexerHelper("  .42{}", realKind, ".42"))
	t.Run("should parse an real token with a leading + and dot", lexerHelper("  +.42{}", realKind, "+.42"))
	t.Run("should parse an real token with a leading - and dot", lexerHelper("  -.42{}", realKind, "-.42"))

	t.Run("should parse a name foo", lexerHelper("  /foo", nameKind, "foo"))
	t.Run("should parse a name /", lexerHelper("  /", nameKind, ""))
	t.Run("should parse a name #", lexerHelper("  /#23", nameKind, "#"))
	t.Run("should parse a name ##", lexerHelper("  /#23#23", nameKind, "##"))
	t.Run("should parse a name #foo", lexerHelper("  /#23#46#4f#4f", nameKind, "#FOO"))

	t.Run("should parse a hex string", lexerHelper("  <464F4F>", stringKind, "FOO"))

	t.Run("should parse a string containing parentheses", lexerHelper("(Strings\nmay contain balanced parentheses () and\nspecial characters (*!&}^% and so on).)", stringKind, "Strings\nmay contain balanced parentheses () and\nspecial characters (*!&}^% and so on)."))
	t.Run("should parse an empty string", lexerHelper("()", stringKind, ""))
	t.Run("should parse a string containing a newline", lexerHelper(`(Strings may contain newlines
and such.)`, stringKind, "Strings may contain newlines\nand such."))

	t.Run("should parse an array start", lexerHelper("[", arrayStartKind, "["))
	t.Run("should parse an array end", lexerHelper("]", arrayEndKind, "]"))

	t.Run("should parse a true token", lexerHelper("  true{}", booleanTrueKind, "true"))
	t.Run("should parse a false token", lexerHelper("  false{}", booleanFalseKind, "false"))

}

func lexerHelper(pdf string, expKind tokenKind, expValue string) func(*testing.T) {
	return func(t *testing.T) {
		l := newLexer(strings.NewReader(pdf))
		tok, err := l.next()
		test.OK(t, err)
		test.Equals(t, &token{expKind, expValue}, tok)
	}
}
