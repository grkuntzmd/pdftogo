/*
 * MIT LICENSE
 *
 * Copyright © 2017, G.Ralph Kuntz, MD.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package parser

import (
	"fmt"
	"strings"
	"testing"

	"bitbucket.org/grkuntzmd/test"
)

func TestParseHeader(t *testing.T) {
	t.Parallel()
	t.Run("should parse a well-formed header", testGoodHeader)
	t.Run("should fail with a missing header", testMissingHeader)
	t.Run("should fail with a version out of range", testPDFVersionOutOfRange)
}

func testGoodHeader(t *testing.T) {
	p, err := NewParser(strings.NewReader("%PDF-1.7\rxxxx\n"))
	test.OK(t, err)

	err = p.parseHeader()
	test.OK(t, err)

	test.Equals(t, float32(1.7), p.Version)
}

func testMissingHeader(t *testing.T) {
	p, err := NewParser(strings.NewReader("xxxx\n"))
	test.OK(t, err)

	err = p.parseHeader()
	test.ExpectedError(t, "cannot find PDF header", err)
}

func testPDFVersionOutOfRange(t *testing.T) {
	p, err := NewParser(strings.NewReader("%PDF-2.0\rxxxx\n"))
	test.OK(t, err)

	err = p.parseHeader()
	test.ExpectedError(t, fmt.Sprintf("PDF version exceeds maximum %0.1f", maxPDFVersion), err)
}
