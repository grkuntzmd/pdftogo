/*
 * MIT LICENSE
 *
 * Copyright © 2017, G.Ralph Kuntz, MD.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package parser

import (
	"bytes"
	"fmt"
	"io"
	"regexp"
	"strconv"

	"bitbucket.org/grkuntzmd/pdftogo/pdf"
)

const (
	trailerByteCount = 1024

	trailerMarker = "trailer"
)

var (
	eofMarker = []byte("%%EOF")
	startXRef = []byte("startxref")

	xrefSubsectionHeaderRegex = regexp.MustCompile(`(\d+) (\d+)`)
	xrefEntryRegex            = regexp.MustCompile(`(\d{10}) (\d{5}) ([nf])`)
)

// parseStartXRefOffset parses the offset to the first xref table and returns it.
func (p *Parser) parseStartXRefOffset() (int64, error) {
	// Seek to trailerByteCount bytes from the end of the PDF.
	skip := maxInt64(0, p.Length-trailerByteCount)

	if _, err := p.Seek(skip, io.SeekStart); err != nil {
		return 0, err
	}

	b := make([]byte, trailerByteCount)
	if _, err := p.Read(b); err != nil {
		return 0, err
	}

	i := bytes.LastIndex(b, eofMarker)
	if i < 0 {
		return 0, fmt.Errorf("cannot find PDF end-of-file marker ('%s')", string(eofMarker))
	}

	// Found the %%EOF. Now look for the 'startxref'.
	i = bytes.LastIndex(b[:i], startXRef)
	if i < 0 {
		return 0, fmt.Errorf("cannot find PDF startxref marker")
	}

	// Found 'startxref'. The next line contains the offset.
	r := newReader(bytes.NewReader(b[i:]))
	if _, err := r.readLine(); err != nil {
		return 0, err
	}
	// The line will contain the offset of the cross reference table as a string.
	line, err := r.readLine()
	if err != nil {
		return 0, err
	}
	off, err := strconv.ParseInt(trimEOL(line), 10, 64)
	if err != nil {
		return 0, err
	}

	return off, nil
}

// parseXRef parses the xref tables.
func (p *Parser) parseXRef() error {
	off, err := p.parseStartXRefOffset()
	if err != nil {
		return err
	}

	p.StartXRefOffset = off

	if err := p.parseXRefTables(); err != nil {
		return err
	}

	// TODO

	return nil
}

// parseXRefSubsection parses a cross reference table subsection (see 7.5.4).
func (p *Parser) parseXRefSubsection() error {
	// Remember the current location.
	off, err := p.Seek(0, io.SeekCurrent)
	if err != nil {
		return err
	}
	count := 0

	// A subsection begins with a line containing two integers.
	r := newReader(p.ReadSeeker)
	line, err := r.readLine()
	if err != nil {
		return err
	}
	count += len(line)

	m := xrefSubsectionHeaderRegex.FindStringSubmatch(line)
	if m == nil {
		return fmt.Errorf("cannot parse xref subsection header")
	}

	obj, err := strconv.ParseInt(m[1], 10, 64)
	if err != nil {
		return err
	}

	ent, err := strconv.ParseInt(m[2], 10, 32)
	if err != nil {
		return err
	}

	// Read all of the subsection entries and parse each one.
	for e := 0; e < int(ent); e++ {
		s, err := r.ReadString('\n')
		if err != nil {
			return err
		}
		count += len(s)

		m := xrefEntryRegex.FindStringSubmatch(s)
		if m == nil {
			return fmt.Errorf("error parsing xref entry: '%s'", s)
		}

		v, err := strconv.ParseInt(m[1], 10, 64)
		if err != nil {
			return err
		}
		offset := v

		v, err = strconv.ParseInt(m[2], 10, 32)
		if err != nil {
			return err
		}
		generation := int(v)

		switch m[3] {
		case "n":
			xrefEntries, ok := p.Document.XRefTable[obj]
			if !ok {
				p.Document.XRefTable[obj] = make([]pdf.XRefEntry, 0, 1)
			}
			p.Document.XRefTable[obj] = append(xrefEntries, pdf.XRefEntry{
				Offset:     offset,
				Generation: generation,
			})

		case "f":

		default:
			return fmt.Errorf("unknown cross reference table entry '%s' at object ID %d", m[3], obj)
		}

		obj++
	}

	_, err = p.Seek(off+int64(count), io.SeekStart)
	return err
}

// parseXRefTable parses a single cross reference table and its associated trailer dictionary (see 7.5.4).
func (p *Parser) parseXRefTable() error {
	// The cross reference table begins with the 'xref' keyword, which has already been peeked. Skip it.
	_, err := p.parseToken()
	if err != nil {
		return err
	}
	if err = p.skipWhitespace(); err != nil {
		return err
	}

	t, err := p.peekToken()
	if err != nil {
		return err
	}

	// Keep reading cross reference subsections until the trialer is reached.
	for t != trailerMarker {
		if err = p.parseXRefSubsection(); err != nil {
			return err
		}

		t, err = p.peekToken()
		if err != nil {
			return err
		}
	}

	// The next line will be 'trailer'.
	if _, err := p.parseToken(); err != nil {
		return err
	}

	// There should be an EOL following the 'trailer' keyword, but some non-conforming documents do not have the EOL (see 7.5.5). Just skip any whitespace (including EOL).
	if err := p.skipWhitespace(); err != nil {
		return err
	}

	// The trailer is a PDF dictionary (see 7.3.7).

	return nil
}

// parseXRefTables parses all of the cross reference tables (and streams) in the PDF (see 7.5.4 and 7.5.8).
func (p *Parser) parseXRefTables() error {
	offset := p.StartXRefOffset
	lastOffset := int64(-1)

	for offset > 0 && offset != lastOffset {
		lastOffset = offset

		if _, err := p.Seek(offset, io.SeekStart); err != nil {
			return err
		}

		if err := p.skipWhitespace(); err != nil {
			return err
		}

		s, err := p.peekToken()
		if err != nil {
			return err
		}

		if s == "xref" {
			if err = p.parseXRefTable(); err != nil {
				return err
			}
		} else {
			// Parse an xref stream
			// TODO
		}
	}

	// TODO

	return nil
}
