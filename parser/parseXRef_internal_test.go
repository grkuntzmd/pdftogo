/*
 * MIT LICENSE
 *
 * Copyright © 2017, G.Ralph Kuntz, MD.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package parser

import (
	"strings"
	"testing"

	"bitbucket.org/grkuntzmd/test"
)

func TestParseStartXRefOffset(t *testing.T) {
	t.Parallel()
	t.Run("should parse a well-formed startxref", testGoodStartXRef)
	t.Run("should fail with a missing EOF marker", testNoEOFMarker)
	t.Run("should fail with a missing startxref", testNoStartXRef)
}

func testGoodStartXRef(t *testing.T) {
	p, err := NewParser(strings.NewReader("%PDF-1.7\rxxxx\nstartxref\n42\r%%EOF\r"))
	test.OK(t, err)

	off, err := p.parseStartXRefOffset()
	test.OK(t, err)

	test.Equals(t, int64(42), off)
}

func testNoEOFMarker(t *testing.T) {
	p, err := NewParser(strings.NewReader("%PDF-1.7\rxxxx\n"))
	test.OK(t, err)

	_, err = p.parseStartXRefOffset()
	test.ExpectedError(t, "cannot find PDF end-of-file marker ('%%EOF')", err)
}

func testNoStartXRef(t *testing.T) {
	p, err := NewParser(strings.NewReader("%PDF-1.7\rxxxx\n%%EOF\r"))
	test.OK(t, err)

	_, err = p.parseStartXRefOffset()
	test.ExpectedError(t, "cannot find PDF startxref marker", err)
}
