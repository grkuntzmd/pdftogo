/*
 * MIT LICENSE
 *
 * Copyright © 2017, G.Ralph Kuntz, MD.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package parser

import (
	"io"
	"strings"
	"testing"

	"bitbucket.org/grkuntzmd/test"
)

func TestReader(t *testing.T) {
	t.Parallel()
	t.Run("should read a line ending in CR", readerHelper("xxxx42\ryyyy", "42\r", 4))
	t.Run("should read a line ending in LF", readerHelper("xxxx42\nyyyy", "42\n", 4))
	t.Run("should read a line ending in CRLF", readerHelper("xxxx42\r\nyyyy", "42\r\n", 4))
}

func readerHelper(pdf, exp string, offset int64) func(*testing.T) {
	return func(t *testing.T) {
		r := strings.NewReader(pdf)

		_, err := r.Seek(offset, io.SeekStart)
		test.OK(t, err)

		rdr := newReader(r)
		l, err := rdr.readLine()
		test.OK(t, err)
		test.Equals(t, exp, l)
	}
}
