/*
 * MIT LICENSE
 *
 * Copyright © 2017, G.Ralph Kuntz, MD.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package parser

import (
	"bufio"
	"fmt"
	"io"
	"strconv"
	"unicode"
)

type tokenKind int

const (
	arrayStartKind tokenKind = iota
	arrayEndKind
	booleanFalseKind
	booleanTrueKind
	integerKind
	nameKind
	realKind
	stringKind
)

// token is the longest string of consecutive characters that represent an atomic value in a PDF document.
type token struct {
	kind  tokenKind
	value string
}

// lexer is a wrapper around the Reader from which tokens are extracted.
type lexer struct {
	*bufio.Reader
}

// newLexer creates a new lexer. The original ReadSeeker is wrapped in a bufio.Reader because we need to be be able to unread one character.
func newLexer(r io.ReadSeeker) *lexer {
	return &lexer{bufio.NewReader(r)}
}

// next returns the next available token from the input stream.
func (l *lexer) next() (*token, error) {
	if err := l.skipWhitespace(); err != nil {
		return nil, err
	}

	c, _, err := l.ReadRune()
	if err != nil {
		return nil, err
	}

	switch {
	case c == '+' || c == '-':
		return l.parseNumber(&c)

	case unicode.IsDigit(c) || c == '.':
		return l.parseNumber(nil)

	case c == '/': // '/' starts a name and continues until a null byte (0x00) or a literal whitespace character or EOF.
		return l.parseName()

	case c == '<': // '<' starts a hexadecimal string.
		return l.parseHexString()

	case c == '(': // '(' starts a string.
		return l.parseString()

	case c == '[': // '[' starts an array.
		return &token{arrayStartKind, "["}, nil

	case c == ']': // ']' ends an array.
		return &token{arrayEndKind, "]"}, nil

	default:
		tok, err := l.parseBoolean(c)
		if tok != nil || err != nil {
			return tok, err
		}

		return nil, fmt.Errorf("unknown token starting with %q", c)
	}
}

func (l *lexer) parseBoolean(c rune) (*token, error) {
	// Peek to check for boolean keywords (true, false).
	peek, err := l.Peek(4)
	if err != io.EOF && err != nil {
		return nil, err
	}

	if c == 't' && string(peek[:3]) == "rue" {
		if _, err = l.Discard(4); err != nil {
			return nil, err
		}

		return &token{booleanTrueKind, "true"}, nil
	}

	if c == 'f' && string(peek) == "alse" {
		if _, err = l.Discard(4); err != nil {
			return nil, err
		}

		return &token{booleanFalseKind, "false"}, nil
	}

	return nil, nil
}

func (l *lexer) parseHexString() (*token, error) {
	var runes []rune

	c, _, err := l.ReadRune()
	if err != nil {
		return nil, err
	}

	for c != '>' {
		hex, err := l.readDigits(true, 2, 16, isHexDigit)
		if err != nil {
			return nil, err
		}

		runes = append(runes, rune(hex))

		c, _, err = l.ReadRune()
		if err != nil {
			return nil, err
		}
	}

	return &token{stringKind, string(runes)}, nil
}

func (l *lexer) parseName() (*token, error) {
	var runes []rune

	c, _, err := l.ReadRune()
	if err != io.EOF && err != nil {
		return nil, err
	}

	for err != io.EOF && !isWhitespace(c) && c != 0 {
		if c == '#' {
			hex, err := l.readDigits(false, 2, 16, isHexDigit)
			if err != nil {
				return nil, err
			}

			runes = append(runes, rune(hex))
		} else {
			runes = append(runes, c)
		}

		c, _, err = l.ReadRune()
		if err != io.EOF && err != nil {
			return nil, err
		}
	}

	if err != io.EOF {
		if err = l.UnreadRune(); err != nil {
			return nil, err
		}
	}

	return &token{nameKind, string(runes)}, nil
}

func (l *lexer) parseNumber(init *rune) (*token, error) {
	var runes []rune
	if init != nil {
		runes = append(runes, *init)
	} else {
		if err := l.UnreadRune(); err != nil {
			return nil, err
		}
	}

	c, _, err := l.ReadRune()
	if err != nil {
		return nil, err
	}

	for unicode.IsDigit(c) {
		runes = append(runes, c)

		c, _, err = l.ReadRune()
		if err != nil {
			return nil, err
		}
	}

	if c != '.' {
		if err = l.UnreadRune(); err != nil {
			return nil, err
		}

		return &token{integerKind, string(runes)}, nil
	}

	runes = append(runes, c)

	c, _, err = l.ReadRune()
	if err != nil {
		return nil, err
	}

	for unicode.IsDigit(c) {
		runes = append(runes, c)

		c, _, err = l.ReadRune()
		if err != nil {
			return nil, err
		}
	}

	if err = l.UnreadRune(); err != nil {
		return nil, err
	}

	return &token{realKind, string(runes)}, nil
}

func (l *lexer) parseString() (*token, error) {
	str, err := l.readString()
	if err != nil {
		return nil, err
	}
	return &token{stringKind, str}, nil
}

func (l *lexer) readDigits(unreadFirst bool, count int, base int, dis func(rune) bool) (int, error) {
	if unreadFirst {
		if err := l.UnreadRune(); err != nil {
			return 0, err
		}
	}

	num := 0
	for i := 0; i < count; i++ {
		c, _, err := l.ReadRune()
		if err != nil {
			return 0, err
		}

		if !dis(c) {
			return 0, fmt.Errorf("unexpect character %q in numeric code", c)
		}

		h, err := strconv.ParseInt(string(c), base, 32)
		if err != nil {
			return 0, err
		}
		num = num*base + int(h)
	}

	return num, nil
}

func (l *lexer) readString() (string, error) {
	var runes []rune

	c, _, err := l.ReadRune()
	if err != nil {
		return "", err
	}

	for {
		switch c {
		case '\\':
			c, _, err = l.ReadRune()
			if err != nil {
				return "", err
			}

			switch c {
			case 'n':
				runes = append(runes, '\n')
			case 'r':
				runes = append(runes, '\r')
			case 't':
				runes = append(runes, '\t')
			case 'b':
				runes = append(runes, '\b')
			case 'f':
				runes = append(runes, '\f')
			case '(':
				runes = append(runes, '(')
			case ')':
				runes = append(runes, ')')
			case '\\':
				runes = append(runes, '\\')
			default:
				octal, err := l.readDigits(true, 3, 8, isOctalDigit)
				if err != nil {
					return "", err
				}

				runes = append(runes, rune(octal))
			}

		case '(':
			runes = append(runes, c)

			str, err := l.readString()
			if err != nil {
				return "", err
			}

			for _, s := range str {
				runes = append(runes, s)
			}

			c, _, err = l.ReadRune()
			if err != nil {
				return "", err
			}

			if c != ')' {
				return "", fmt.Errorf("unbalanced parentheses in string")
			}

			runes = append(runes, c)

		case ')':
			if err = l.UnreadRune(); err != nil {
				return "", err
			}

			return string(runes), nil

		default:
			runes = append(runes, c)
		}

		c, _, err = l.ReadRune()
		if err != nil {
			return "", err
		}
	}
}

func (l *lexer) skipWhitespace() (err error) {
	c, _, err := l.ReadRune()
	if err != nil {
		return
	}

	for isWhitespace(c) || c == '%' {
		if c == '%' {
			// Skip past the comment.
			c, _, err = l.ReadRune()
			if err != nil {
				return
			}

			for !isEOL(c) {
				c, _, err = l.ReadRune()
				if err != nil {
					return
				}
			}
			continue
		}

		c, _, err = l.ReadRune()
		if err != nil {
			return
		}
	}

	err = l.UnreadRune()
	return
}

func isHexDigit(r rune) bool {
	return isOneOf(r, "0123456789ABCDEFabcdef")
}

func isOctalDigit(r rune) bool {
	return isOneOf(r, "01234567")
}
