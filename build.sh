#!/bin/sh

cd "${0%/*}"

buildInfo="`date -u '+%Y-%m-%dT%TZ'`|`git describe --always --long`|`cat VERSION`"
go install -ldflags "-X main.buildInfo=${buildInfo} -s -w" ./cmd/...